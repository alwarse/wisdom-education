package com.education.common.constants;

/**
 * @author zengjintao
 * @create_at 2021年10月16日 0016 13:56
 * @since version 1.0.3
 */
public interface LocalQueueConstants {

    String USER_LOGIN_SUCCESS_QUEUE = "userLoginSuccessQueue";

    String SYSTEM_MESSAGE = "systemMessageQueue";
}
